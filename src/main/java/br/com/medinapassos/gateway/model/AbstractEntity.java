package br.com.medinapassos.gateway.model;

import java.io.Serializable;

/**
 * @author William Suane
 */
public interface AbstractEntity extends Serializable {
    Long getId();
}
